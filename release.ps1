# This script make 3 zips, KSRSS-16K, KSRSS-8K and Earth-64K
# You need NVIDIA Texture Tools 3, ImageMagick and 7-Zip to run it
$versionNumber = 0.7
$zip = "C:\Program Files\7-Zip\7z.exe"
Remove-Item release$versionNumber-16K -Recurse -ErrorAction Ignore
Remove-Item release$versionNumber-64K -Recurse -ErrorAction Ignore
Remove-Item release$versionNumber-8K -Recurse -ErrorAction Ignore
Remove-Item KSRSS-8K-$versionNumber.zip -ErrorAction Ignore
Remove-Item KSRSS-16K-$versionNumber.zip -ErrorAction Ignore
Remove-Item Earth-64K-$versionNumber.zip -ErrorAction Ignore
mkdir release$versionNumber-16K
cd release$versionNumber-16K
mkdir GameData
cd ..
Copy-Item .\KSRSS release$versionnumber-16K\GameData\KSRSS -Recurse
Copy-Item .\Extras release$versionnumber-16K -Recurse
Copy-Item .\README.md release$versionNumber-16K
&$zip a KSRSS-16K-$versionNumber.zip -tzip -r .\release$versionnumber-16K\GameData
&$zip a KSRSS-16K-$versionNumber.zip -tzip -r .\release$versionnumber-16K\Extras
&$zip a KSRSS-16K-$versionNumber.zip -tzip -r .\release$versionnumber-16K\README.md
Remove-Item release$versionNumber-16K -Recurse

mkdir release$versionNumber-8K
cd release$versionNumber-8K
mkdir GameData
cd ..
Copy-Item .\KSRSS release$versionnumber-8K\GameData\KSRSS -Recurse
&.\scaleTextures.ps1 -currentFolder "$PSScriptRoot\release$versionnumber-8K\GameData\KSRSS\Textures\PluginData"
&.\scaleTextures.ps1 -currentFolder "$PSScriptRoot\release$versionnumber-8K\GameData\KSRSS\EVE\Textures\Main\Earth" -filter *.dds
&.\scaleTextures.ps1 -currentFolder "$PSScriptRoot\release$versionnumber-8K\GameData\KSRSS\EVE\Textures\Main\JupiterClouds\JupiterTemperate.dds"
&.\scaleTextures.ps1 -currentFolder "$PSScriptRoot\release$versionnumber-8K\GameData\KSRSS\EVE\Textures\Main\NeptuneClouds\NeptuneTemperate.dds"
&.\scaleTextures.ps1 -currentFolder "$PSScriptRoot\release$versionnumber-8K\GameData\KSRSS\EVE\Textures\Main\NeptuneClouds\NeptuneTropical.dds"
&.\scaleTextures.ps1 -currentFolder "$PSScriptRoot\release$versionnumber-8K\GameData\KSRSS\EVE\Textures\Main\SaturnClouds" -filter Equatorial*.dds
&.\scaleTextures.ps1 -currentFolder "$PSScriptRoot\release$versionnumber-8K\GameData\KSRSS\EVE\Textures\Main\SaturnClouds" -filter Temperate*.dds
&.\scaleTextures.ps1 -currentFolder "$PSScriptRoot\release$versionnumber-8K\GameData\KSRSS\EVE\Textures\Main\SaturnClouds" -filter Poles*.dds
&.\scaleTextures.ps1 -currentFolder "$PSScriptRoot\release$versionnumber-8K\GameData\KSRSS\EVE\Textures\Main\SaturnClouds" -filter Tropical*.dds
&.\scaleTextures.ps1 -currentFolder "$PSScriptRoot\release$versionnumber-8K\GameData\KSRSS\EVE\Textures\Main\SaturnClouds" -filter Hexagon*.dds
Copy-Item .\Extras release$versionnumber-8K -Recurse
Copy-Item .\README.md release$versionNumber-8K
&$zip a KSRSS-8K-$versionNumber.zip -tzip -r .\release$versionnumber-8K\GameData
&$zip a KSRSS-8K-$versionNumber.zip -tzip -r .\release$versionnumber-8K\Extras
&$zip a KSRSS-8K-$versionNumber.zip -tzip -r .\release$versionnumber-8K\README.md
Remove-Item release$versionNumber-8K -Recurse

mkdir release$versionNumber-64K
cd release$versionNumber-64K
mkdir GameData
cd ..
Copy-Item .\KSRSS-64K release$versionNumber-64K\GameData\KSRSS-64K -Recurse
Copy-Item .\README.md release$versionNumber-64K
&$zip a Earth-64K-$versionNumber.zip -tzip -r .\release$versionnumber-64K\GameData
&$zip a Earth-64K-$versionNumber.zip -tzip -r .\release$versionnumber-64K\README.md
Remove-Item release$versionNumber-64K -Recurse